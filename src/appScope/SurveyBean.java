package appScope;

public class SurveyBean {
	private int javaQuality = 0;
	private int csQuality = 0;

	public int getJavaQuality() {
		return javaQuality;
	}

	public int getCsQuality() {
		return csQuality;
	}

	public void setQuality(String bookTitle) {
		try {
			if (bookTitle.equals("java")) {
				javaQuality++;
			}
			if (bookTitle.equals("c")) {
				csQuality++;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
